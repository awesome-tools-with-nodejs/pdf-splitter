### Description
This repository is built to learn express framework and create some tools for later use

### How to use
Build docker
```bash
docker build . -t tools:1.0
```
Development environment
```bash
docker run -it -p 50000:50000 -v $PWD:/app tools:1.0 /bin/bash
```

### Run commands in docker
```bash
npm install .
npm start
```

### Use tools
To go your browser and browse to 
```bash
http://localhost:50000/
```
### TODO
- [ ] Authorization page with JWT & database management
- [ ] Split into multiple container with microservice structure
- [ ] Find projects with better use of socket.io
