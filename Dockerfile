FROM node:18

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

RUN apt-get update && apt-get install pdftk -y


COPY package*.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

ENV PORT 50000
EXPOSE $PORT
CMD [ "npm", "start" ]
