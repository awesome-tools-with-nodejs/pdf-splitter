var express = require('express');
var router = express.Router();

// /tool/realtime
router.get('/', function (req, res, next) {
  res.sendFile('/app/views/realtime.html');
});

module.exports = router;
