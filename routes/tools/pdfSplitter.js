var express = require('express');
var router = express.Router();
const multer = require('multer');
var scissors = require('scissors');
var fs = require('fs');
const fsExtra = require('fs-extra')

// SET STORAGE
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'tmp/splitter')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
})

var upload = multer({ storage: storage })

// /tool/pdfSplitter
router.get('/', function (req, res, next) {
  res.sendFile('/app/views/pdfSplitter.html');
});

// /tool/pdfSplitter/upload
router.post('/upload', upload.single('upfile'), function (req, res, next) {
  // Get file from request
  const file = req.file
  const fielPath = 'tmp/splitter/' + file.originalname;
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }

  // Check file extension
  if (req.file.mimetype != "application/pdf")
    res.status(200).end("Please load pdf file");

  // Get request
  var pageFrom = parseInt(req.body.pageFrom);
  var pageTo = parseInt(req.body.pageTo);
  var pdf = new scissors(file.path);
  var maxPage;
  pdf.getNumPages().then(maxPage);
  // console.log(pageFrom, pageTo, maxPage)
  
  // Limit the request page number
  pageTo = Math.min(maxPage, pageTo);
  pageFrom = Math.min(pageFrom, maxPage);
  if (maxPage == 0) {
    res.status(200).end("Your file has zero page")
  }
  var pdfSplitted = pdf.range(pageFrom, pageTo)

  // Put them into stream
  pdfSplitted.pdfStream()
    .pipe(fs.createWriteStream(fielPath))
    .on('finish', function () {
      console.log("We're done!");
      res.download(fielPath, function (req, res) {
        fsExtra.emptyDirSync("tmp/splitter");
      })
    }).on('error', function (err) {
      throw err;
    });
});

module.exports = router;
